const express =require("express");
// Mongoose is a package that allows creation of Schema to model ourdata structure
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3009;

// [ SECTION ] MongoDB Connection

	// mongoose.connect("<MongoDB connecting string>", { useNewUrlParser: true });

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.hj52sqb.mongodb.net/s35-discussion", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

	// The { newUrlParses : true } allows us to avoid any current and futures errors while connecting to MongoDB.
	// The { useUnifiedTopology : true } allows us to enable the new MongoDB connection with the configuration option while establishing MongoDB server and Mongoose.

// Connection to the Database

	// Allows us to handle errors when the initial connection is established

let db = mongoose.connection

// If a correction error occurred, output in the console.
// console.error.bind(console) allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection error"));

// If the connexion is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."))

//[ Mongoose Schemas ]

	// Schemas determine the structure of the documents to be written in the database
	// Schemas acts as blueprint to our data
	// Use the schema() constructor of the Mongoos module to create a new Schema object
	// The "new" keyword creates in new schema 

const taskSchema = new mongoose.Schema({
	// Define the fields with the corresponding data type 
	// For a task, it needs a "task name" and "task status"
	// There should be a field called "name" and its data type is "String"
	name: String,
	status: {
		type: String,
		// Default values are the pre defined values for a field if we don't put any value
		default: "pending"
	}
})

// [ Models ]

	// It uses Kmart and are used to create/instantiate objects that correspond to this schema 
	// It acts like as a middleman from the server (JS CODE) to our database 
	// Server > Schemer (Blueprint) > Database > Collection

const Task = mongoose.model("Task", taskSchema);

// [ Creation of todo list routes ]

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

	// Creating a new Task

	// Business Logic

	/*
		1. Add a functionality to check if there are duplicate task
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database 
		2. The task data will be coming from the request's body 
		3. Create a new task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema by default is for it to be "pending"
	*/

app.post("/tasks", (req, res) => {

	// cheques if there are duplicate tasks 
	// "findOne" is a mongoose method that acts similar to "find" or Mongodb 
	// "findOne return the first document that matches the search criteria as single object 
	Task.findOne({name: req.body.name}).then((result, err) => {

		// If a document was found and the documents name matches the information sent via the client/postman 
		if(result != null && result.name == req.body.name){

			// Return a message to the client/postman 
			return res.send("Duplicate task found");

		} else{

			// Create a new task and save it to the database 
			let newTask = new Task({
				name: req.body.name
			});

			// "save()" method will store the information to the database 
			newTask.save().then((savedTask, saveErr) =>{

				// If there are errors in saving 
				if(saveErr){
					return console.error(saveErr);
				} else{

					// Will print any errors found in the console
					return res.status(201).send("New task created!");

				}
			})
		}
	})

})
// Getting all the task 
	
	//Business Logic 

	/*
		1. Retrieve all the documents
		2. If an error is encountered, print error
		3. If no error are found, send a success status back to the client/Postman and return an array of documents
	*/
app.get("/tasks", (req, res) => {

	// "find" is a Mongoose Method that is similar to MongoDB 'find', and empty "{}" means it returns all documents and stores them in the "result" parameter of the callback function
 
	Task.find({}).then((result,err) =>{

		//If an error occured
		if(err){

			// To print any errors found in the console
			return console.log(err);

		// If no error are found
		} else{

			// Return the result
			// The "json" method allows to send a JSON format for the response
			// The returned response is purposefully returned as an object with the "data" property to mirror the real world complex data structures

			return res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, () => console.log(`Server running at port ${port} `));