const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [ true, "Name is required." ]
		},
		email: {
			type: String,
			required: [ true, "Email is required." ]
		},
		password: {
			type: String,
			required: [ true, "Password is required."]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		orderedProduct : [
		{
			products: [
			{
				productId: {
					type: String,
					required: [ true, "Product Id is required"]
				},

				productName: {
					type: String,
					required: [ true, "Product Name is required"]
				},

				quantity: {
					type: Number,
					required: [true, "Quantitiy is required"]
				}

			} 
			],
			totalAmount: {
				type: Number,
				required: [ true, "Total Amount is required."]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
		]
})

module.exports = mongoose.model("User", userSchema);