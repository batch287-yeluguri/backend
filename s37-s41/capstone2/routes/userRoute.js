const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth")

router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/authenticate", (req, res) => {
	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));
});
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);// WIll return the token.payload

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({ userId: userData._id }).then(resultFromController => res.send(resultFromController));
});

module.exports = router;