const express = require("express");
const router = express.Router();
const courseController = require("../controllers/productController")
const auth = require("../auth")

router.get("/all", (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


module.exports = router;
