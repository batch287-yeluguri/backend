const User = require("../models/User");
const Course = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth")

module.exports.registerUser = (reqBody) => {

	console.log(reqBody)

	let newUser = new User({
		name: reqBody.name,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, abc) => {

		if(abc){
			return false
		} else {
			return true
		};
	});
};

module.exports.authenticateUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			} else {

				//Allows th eapplication to proceed with the next middleware function/ callback function in the route
				return false
			};
		};
	});
};

module.exports.getProfile = (data) => {

	console.log(data);// Will return the key/value of pair of userId: data.id

	return User.findById(data.userId).then(result => {
		result.password = "";


		return result;
	});
};

