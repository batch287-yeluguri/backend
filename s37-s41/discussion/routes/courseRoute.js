const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController")
const auth = require("../auth");

// Route for creating a course
//my code
// router.post("/", auth.verify, (req, res) => {
// 	const courseData = auth.decode(req.headers.authorization);
// 	console.log(courseData);
// 	courseController.addCourse(courseData.isAdmin,req.body).then(resultFromController => res.send(resultFromController));
// });

// solution
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {

	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

router.get("/active-courses", (req, res) => {

	courseController.aciveCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific course
router.get("/:courseId", auth.verify, (req, res) => {

	//console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

router.patch("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


module.exports = router;