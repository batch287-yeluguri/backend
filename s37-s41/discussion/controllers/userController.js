const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		
		// Thee "find" method retuns a record if a match is found
		if(result.length > 0){
			
			return true

		// No duplicate emails found
		// The user is not yet registered in the database
		} else {

			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {

	console.log(reqBody)

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, abc) => {

		if(abc){
			return false
		} else {
			return true
		};
	});
};

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		if(result == null){

			return false;

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			} else {

				//Allows th eapplication to proceed with the next middleware function/ callback function in the route
				return false
			};
		};
	});
};

module.exports.getProfile = (data) => {

	console.log(data);// Will return the key/value of pair of userId: data.id

	return User.findById(data.userId).then(result => {
		result.password = "";
		// Returns the user information with the password as an empty string
		return result;
	})
}

module.exports.enroll = async (data) => {

	// 1st Await Update first the User Models
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({ courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	} else{
		return false;
	};
}