const express = require("express");
const mongoose = require("mongoose")

const userRoutes = require("./routes/userRoute.js");

//Allows us to controll the app's Cross Origin Resource Sharing settings
// Allow our backend application to be available to our frontend application
const cors = require("cors")

// Create an "app" variable that stores the result of the express function 
const app = express();

// Connecting to MongoDB Atlas
	mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.hj52sqb.mongodb.net/s37-s41", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);

mongoose.connection.once("open", () => console.log('Now connected in the cloud.'))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Will used the defined port number for the application whenever the variable is available or will use port 4000 if none is defined 
// Doing this syntax, it will allow flexibility when using the application locally or as a hosted application.

app.listen(process.env.Port || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});
app.use("/users", userRoutes);