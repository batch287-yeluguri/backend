const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.getUsers = () => {

	return User.find({}).then(result => {
		return result;
	});
}
module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, abc) => {

		if(abc){
			console.log(error)
			return false
		} else {
			return true
		}
	})
}


module.exports.userDetails = (reqBody) => {

	return User.findOne({ _id: reqBody._id }).then(result => {
		if(result == null){
			return false;
		} else {
			return result
		}
	});
};
