const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")

router.post("/", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/details", (req, res) => {
	userController.userDetails(req.body).then (resultFromController => res.send(resultFromController));
})
module.exports = router;