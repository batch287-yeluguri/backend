let titles = [];
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
	titles.push(data.map((item) => item.title));
	console.log(titles);
})


fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
	.then((json) => {
		console.log(json)
	console.log('The item "' + json.title+'" on the list has a status of '+json.completed)
})


fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId: 1
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: "Updated To Do List Item",
		description: "To update the my to do list with a different data structure",
		status: "Pending",
		dateCompleted: "Pending",
		userId:1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			dateCompleted: "07/09/21",
			status: "Complete"
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));
fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'DELETE'
	});