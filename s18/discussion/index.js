console.log("Hello World?");

//[ SECTION ] Functions

	// [ SUB-SECTION ] Parameters and Arguments

	// Functions in JS are lines/ blocks of codes that tells our device/ application/ browser to perform a cretaain task when called/ invoked/ triggered.

	function printInput(){
		let nickname = prompt("Enter your nickname:");
		console.log("Hi, "+nickname);
	};

	//printInput();

	//For other cases, functions can also process data directly passed into it instead of relying only on Global Variable and prompt().

	//name112313131313 ONLY ACTS LIKE A VARIABLE

		function printName(name112313131313){// name112313131313 is the parameter
			// This one should work as well:
				//let name = name112313131313;
				//console.log("Hi, "+name);
			console.log("Hi, "+ name112313131313);
		};

			//console.log("Hi, "+name112313131313); //This one is error as it is not defined.

		printName("Juana");

		// You can directly pass data into the function. The function can then call/ use that data which is reffered as "name112313131313" within the functon.

		printName("Nirupama");

		// When the printName(); function is called again, it stores the value of "Carlo" in the parameter "name112313131313" then use it to print a message.

		//Variables can also be passed as an argument.
		let sampleVariable = "Yui";

		printName(sampleVariable);

		//Function arguments cannot be used by a function if there are no parameters provided within the function.

		printName(); //It will be Hi!, undefined

		function checkDivisibilityBy8(num){
			let	remainder = num % 8;
			console.log("The remainder of "+num+ " divided by 8 is: "+remainder);
			let isDivisibleBy8 = remainder ===0;
			console.log("Is "+ num+ " divisible by 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8("64");
		checkDivisibilityBy8(52);

		// You can also do the same using prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

	// Functions as Arguments 

		//Function parameters can also accept other function as arguments

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.")
		};

		// function invokedFunction(argumentFunction){
		// 	argumentFunction();
		// };

		function invokeFunction(iAmNotRelated){
			//console.log(iAmNotRelated);
			iAmNotRelated();
			argumentFunction();
		};

		invokeFunction(argumentFunction);

		//Mini-Activity:
		//Goal to do, invoke argumentFunction using invokeFunction.

		//Adding and removing the paranthesis "()" impacts the output of JS heavily.

		//console.log(argumentFuntion);

		// Using nultiple parameters
			// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

		function createFullName(firstName, middleName, lastName){
			console.log(firstName+ " "+middleName+" "+lastName);
			console.log(middleName+ " "+lastName+" "+firstName);
		};

		createFullName("Mike", "Kel", "Jordan");

		createFullName("Mike","Jordan");

		createFullName("Mike","Kel", "Jordan", "Mike");

		// In JS, providing more/ less arguments than the expected parameters will not return an error.

		// Using Variable as Arguments

		let firstName = "Dwayne";
		let secondName = "The Rock";
		let lastName = "Johnson";

		createFullName(firstName, secondName, lastName);

// [ SECTION ] The Return Statement
	
	// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

		function returnFullName(firstName, middleName, lastName){
			return firstName+ " " + middleName + " " + lastName;
			console.log(firstName+ " "+middleName+" "+lastName);
			console.log(firstName+ " "+middleName+" "+lastName);
			console.log(firstName+ " "+middleName+" "+lastName);
		}
		let completename= returnFullName("Jeffrey", "Smith", "Bezos");
		console.log(completename);

		//In our example, the "returnFullName" function was invoked called in the same line as declaring a variable.

		//Whatever value is returned from the "returnFullName" funtion is stored in the "completeName" variable.
		// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.

		//In this example, console.log() will print the returned value of the returnFullName() function.

		console.log(returnFullName(firstName, secondName, lastName));

		function returnAddress(city, country){
			let fullAddress = city + ", "+country;
			return fullAddress;
		};

		let	myAddress = returnAddress("Manila City" , "Philippines");
		console.log(myAddress);

		function printPlayerInfo(username, level, job){
			console.log("Username: " + username);
			console.log("Level: " + level);
			console.log("Job: " +job);
		};

		let	user1 = printPlayerInfo("knight_white", 95, "Paladin");
		console.log(user1);

		// Returns undefined because printPlayerInfo return nothing. It ony console.log the details.

		
