// Defines WHEN particular controllers will be used
// Contains all tge endpoints for our application

// We seperate the routes such that "app.js" only contains info on the server
// We will need to use express Router() function to achieve this
const express = require("express");

// Creates a Router instance that function as a middleware and routing system

const router = express.Router();
const taskController = require("../controllers/taskController")

// [ SECTION ] Routes

// Routes to get all the tasks
router.get("/", (req, res) => {

	// Invoke the "getAllTasks()" function from the "controller.js" file and return back to th eclient/Postman

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});


// Delete Task
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id, req.body).then(
		resultFromController => res.send(resultFromController));
})
// Update Task

router.put("/:id", (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(
		resultFromController => res.send(resultFromController));
})

module.exports = router;