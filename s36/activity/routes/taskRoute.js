
const express = require("express");

const router = express.Router();
const taskController = require("../controllers/taskController")


router.get("/", (req, res) => {


	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.get("/:id", (req, res) => {


	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});




router.put("/:id/complete", (req,res) => {
	taskController.changeStatusTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;