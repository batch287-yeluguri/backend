

const Task = require("../models/task");

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result;
	});
};

module.exports.getSpecificTask = (taskId) => {

	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return result
		}
	});
};

module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {

		if(error){
			console.log(error);
			return false
		} else {
			return task
		};
	});
};



module.exports.changeStatusTask= (taskId) =>{

	return Task.findByIdAndUpdate(taskId).then((result, err) =>{
			if(err){
				console.log(err);
				return false;
			}
			result.status = "Complete";


		return result.save().then((updatedTask,saveErr) =>{

			if(saveErr){

				console.log(err)
				return false
			} else {
				return updatedTask;
			};
		});

	});
}