console.log("Hello World?");

// [ SECTION ] Objects
	// An object is a data type that is used to represent real world objects 
	// Information stored in ibjects are represented in a "key: value" pair

	// Creating object using object initializers/ literal notation

	// Syntax:
		// let objectName = {
			// keyA : valueA,
			// keyB : valueB
		//};
	
	let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: 1999
	};

	console.log('Creating object using object initializers/literal notation:');
	console.log(cellphone);
	console.log(typeof cellphone);

	// Creating object using a contructor function

		// Create a reusable function to create several object that have the same data structure

		// Syntax
			//function objectNAme(keyA, keyB){
				// this.keyA : valueA,
				// this.keyB : valueB
			//}

		//THis is an object
		// The 'this' keyword allows us to assign a new object's properties by associating them with values recieved from a constructor function's parameters

	function Laptop(name, manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	};

	// The 'new' operator creates an instance of an object

	let laptop = new Laptop('Lenovo', 2008);
	console.log("Result of creating object using a constructors: ");
	console.log(laptop);

	let mylaptop = new Laptop('Macbook', 2022);
	console.log("Result of creating object using a constructors: ");
	console.log(mylaptop);

	function Shirt (color, day){
		this.colorSelected = color;
		this.daySelected = day;
	};

	let shirt = new Shirt ('Red', 'Monday');
	console.log('Result of creating object using a constructors: ');
	console.log(shirt);

	let dress = new Shirt ('Blue', 'Tuesday');
	console.log('Result of creating object using a constructors: ');
	console.log(dress);

	let socks = new Shirt ('Yellow', 'Thursday');
	console.log('Result of creating object using a constructors: ');
	console.log(socks);

		// Unable call that new operators it will return undefined without the 'new' operator 
		// The behaviour for this is like calling/ invoking the Laptop function instead of creating a new object instance.

	// Create empty objects 

	let computer = {};
	let myComputer = new Object();

	// [ SECTION ] Accessing Object Properties

	// Using the dot notation
	console.log('Result from dot notation: '+shirt.colorSelected);

	//Using the square bracket notation
	console.log('Result from square bracket notation: '+shirt['daySelected']);

	// Accessing Array Objects

		// Accessing array elements can be also done using square brackets 

	let array = [ shirt, dress ];

	console.log	(array[0]['colorSelected']);
	// This tells us that array[0] is an object by using the dot notation
	console.log(array[0].daySelected);

	// Initializing/ Adding object properties using dot notation

	let car ={}

	car.name = 'Honda Civic';
	console.log("Result from Adding object properties using dot notation");
	console.log(car);

	// Initializing/ Adding object properties using bracket notation

	car['manufacture date'] = 2023;
	console.log(car['manufacture date']);
	console.log(car['manufacture Date']);
	console.log(car.manufacturedate);
	console.log("Result from Adding object properties using bracket notation");
	console.log(car);

	// Deleting object properties
	delete car['manufacture date'];
	console.log('Result from deleting properties');
	console.log(car);

	//Reassigning object properties
	car.name = 'Dodge Charger R/T';
	console.log('Result from Reassigning properties');
	console.log(car);

// [ SECTION ] Object Methods 
	// A method is a function which is a property of an object 

	let person = {
		name: 'John',
		talk: function(){
			console.log('Hello my name is '+name);
		}
	};

	console.log(person);
	console.log('Result from object method: ');
	person.talk();

	// Methods are useful for creating reusable functions that perform tasks related to object

	let friend = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: 'Austin',
			country: 'Texas'
		},
		email: ['joe@mail.com', 'joesmith@mail.xyz'],
		introduce: function(){
			console.log('Hello my name is '+this.firstName+" "+ this.lastName);
		},
		introduceAddress: function(){
			console.log('I live in '+this.address.city+', '+this.address.country);
		},
		introduceContactDetails: function(){
			console.log('You may contact me on '+this.email[0]+' or '+ this.email[1]);
		}
	};

	friend.introduce();
	friend.introduceAddress();
	friend.introduceContactDetails();

	// [ SECTION ] Real World Application of Objects

		/* 
		Scenario 
			1. We would like to create a game that would have several pokemon interact with each other
			2.  Every pokemon would have the same set os sats, properties and functions
		*/

	// Using objects literals to create multiple kinds of pokemon would be time consuming

	let myPokemon = {
		name: 'Pikachu',
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log('This Pokemon tacked targetPokemon');
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth");
		},
		faint: function(){
			console.log('Pokemon fainted!');
		}
	}

	console.log(myPokemon);

	function Pokemon(name, level){
		//Properties
		this.name = name;
		this.love = level;
		this.health = 2* level;
		this.attack = level;

		//Methods
		this.tackle	= function(target){
			console.log(this.name +" tackled "+ target.name)
			console.log("targetPokemon's health is now reducedto _targetPokemonHealth_")
		};
		this.faint = function(){
			console.log(this.name + ' fainted ');

		};
	};

	let pikachu = new Pokemon("Pikachu", 16);
	let rattata = new Pokemon("Rattata", 8);

	pikachu.tackle(rattata);
	rattata.faint(); 





	