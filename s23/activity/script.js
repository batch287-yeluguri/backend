// let myPokemon = {
// 		name: 'Pikachu',
// 		level: 3,
// 		health: 100,
// 		attack: 50,
// 		tackle: function(){
// 			console.log('This Pokemon tacked targetPokemon');
// 			console.log("targetPokemon's health is now reduced to _targetPokemonHealth");
// 		},
// 		faint: function(){
// 			console.log('Pokemon fainted!');
// 		}
// 	}

// 	console.log(myPokemon);

// 	function Pokemon(name, level){
// 		//Properties
// 		this.name = name;
// 		this.love = level;
// 		this.health = 2* level;
// 		this.attack = level;

// 		//Methods
// 		this.tackle	= function(target){
// 			console.log(this.name +" tackled "+ target.name)
// 			console.log("targetPokemon's health is now reducedto _targetPokemonHealth_")
// 		};
// 		this.faint = function(){
// 			console.log(this.name + ' fainted ');

// 		};
// 	};

// 	let pikachu = new Pokemon("Pikachu", 16);
// 	let rattata = new Pokemon("Rattata", 8);

// 	pikachu.tackle(rattata);
// 	rattata.faint(); 

let trainer ={
	name: "Ash Ketchum",
	age:10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log("Pikachu! I chose you!");
	}
}

console.log(trainer);
console.log('Result of dot notation: ');
console.log(trainer.name);

console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);
console.log("Result of talk method");
trainer.talk();

function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.faint = function(target){
			console.log(target.name + ' fainted ')
	};
	this.tackle = function(target){
		console.log(this.name +" tackled "+ target.name)
		target.health = target.health - this.attack;
		console.log(target.name + "'s health is name reduced to "+ target.health);
		if( target.health <= 0){
			this.faint(target);
		}
	};
	
};

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);
let mewtwo = new Pokemon("Mewtwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);









