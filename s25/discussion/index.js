console.log("Hello World?")

// [ SECTION ] JSON Objects
	
	 // JSON stands for JavaScript Object Notation
	 // Syntax:
	 	/*
	 	{
			"propertyA": "valueA",
			"propertyB": "valueB"
	 	}
	 	*/

	 // JSON Objects 
	 /*
	 	{
			"city": "Manila",
			"province": "Metro Manila",
			"country": "Philippines"
	 	}
	 */

 // [ SECTION ] JSON Arrays

 	/*
 		"cities": [
	 		{
	 			"city": "Manila City", 
	 			"province": "Metro Manila",
	 			"country": "Philippines" 
	 		},
 		{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines" },
 		{"city": "Makati City", "province": "Metro Manila", "country": "Philippines" }
 		]
 	*/

// [ SECTION ] JSON Methoda
	// The JSON Object methods for parsing and converting data into stringified JSON

	//Converting Data into Stringified JSON
		// Stringified JSON is a JavaScript object converted into a string to be used in other function of a JavaScript application

	let batchesArr = [{ batchName: 'Batch X'}, { batchName: 'Batch Y'}];

	// The "stringify" method is used to convert JavaScript Objects into a string

	console.log("Result from strigify method:");
	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name: "John",
		age: 31,
		address: {
	 			"city": "Manila City", 
	 			"country": "Philippines" 
		}
	});

	console.log(data);

// [ SECTION ] Using Stringify Method with Variables
	//When information is stored in a variable and is not hard coded into an object that is being stringified, we can supply the value with a variable.

	// Syntax:
	/*	
		JSON.stringify({
			propertyA: "variableA",
			propertyB: "variableB"
		})
	*/
	// Since we do not have a frontend application yet, we will use the prompt method in order to gather user data to be supplied to the user deatails

	//User details
		// let firstName = prompt("What is your first name?");
		// let lastName = prompt("What is your last name?");
		// let age = prompt("What is your age?");
		// let address = {
		// 	city: prompt("Which city do you live in?"),
		// 	country: prompt("Which country does your city belong to?")
		// };
		// let otherData = JSON.stringify({
		// 	firstName: firstName,
		// 	lastName: lastName,
		// 	age: age,
		// 	address: address
		// });

		// console.log(otherData);
// [ SECTION ] Converting stringified JSON into JavaScript objects
	// This happens both for sending information to a backend application and sending information back to a frontend application

		let batchesJSON = `[{ "batchName": "Batch X"}, { "batchName": "Batch Y"}]`;
		console.log(batchesJSON);
		console.log("Result from parse method:");
		console.log(JSON.parse(batchesJSON));

		
		// Parse the following into a JavaScript object:

		let stringfiedObject = `{"name":"John","age":31,"address":{"city":"Manila","country":"Philippines"}}`

		console.log(JSON.parse(stringfiedObject));
		console.log(stringfiedObject);


