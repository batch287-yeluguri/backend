const http = require('http');
const port = 3000;

const server = http.createServer((request,response) => {


	if (request.url == '/login'){

		response.writeHead(200, {'constant-Type': 'text/plain'})
		response.end('You are in the Login Page!')
	
	}else{
		response.writeHead(404, {'constant-Type': 'text/plain'})
		response.end('Page not available!')
	}
})
server.listen(port);

console.log(`Server is now accessible at localhost:${port}.`);