const http = require('http');

const port = 4004;

// Creates a variable "server" that sores the output of the 'createServer' method

const server = http.createServer((request,response) => {

	if (request.url == '/greetings'){

		response.writeHead(200, {'constant-Type': 'text/plain'})
		response.end('Welcome to the server! This is currently running at local host: 4004.')
	
	} else if (request.url === '/homepage'){

		response.writeHead(200, {'constant-Type': 'text/plain'})
		response.end('Welcome to the homepage! This is currently running at local host: 4004.')
	}else{
		response.writeHead(404, {'constant-Type': 'text/plain'})
		response.end('Page not available!')
	}
})

// Uses the "server and "port" variables created above
server.listen(port);

console.log(`Server is now accessible at localhost:${port}.`);