const express = require("express");
const app = express();
const port = 3000;
app.use(express.json())
app.use(express.urlencoded({extended:true}));

app.get("/home", (request, response) => {
	response.send('Welcome to the home page');
});

let users = [];

 users =[{
	"username": "johndoe",
	"password": "johndoe1234"
}];
app.get("/users", (request, response) => {
	response.send(users);
});

app.delete("/delete-user", (request, response) => {
	console.log(request.body)
	for( let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){

			users.splice(i,1);
			console.log(users);

			message = `User ${request.body.username}'s has been deleted`;
			console.log("Newly updated mock database:");
			console.log(users);
			break;
		} else {
			message = "User does not exist."
		}

	}

	response.send(message);
});

app.listen(port, () => console.log( `Server is currently running at port ${port}`));