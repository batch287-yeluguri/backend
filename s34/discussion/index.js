// Use the required directive to load the express module/package
// It allows us to access to methods and function that will allow us to easily create a server
const express = require("express");

//Creates an application using express
// In layman's term, app is our server
const app = express();

//For our application server to run, we need a port to listen to 
const port = 3001;

// Use middleware to allow express to read JSON
app.use(express.json())

// Use middleware to alloe express to able to read more data types from a response 
app.use(express.urlencoded({extended:true}));

// [SECTION] Routes
	// express has methods corresponding to each HTTP method

app.get("/hello", (request, response) => {
	response.send('Hellow from the /hello endpoint!');
});

app.post("/display-name", (req, res) =>{
	console.log(req.body);
	res.send(`Hellow there ${req.body.firstName} ${req.body.lastName}!`);
});

//Sign-up in our Users

let users = []; // This is our mockdatabase

app.post("/signup", (request,response)=> {
	console.log(request.body);

	// If contents of the "request body" with the property "username" and "password" is not empty
	if(request.body.username !== "" && request.body.password !== ""){

		// This will sttore the user object sent via Postman to the users array areated above
		users.push(request.body)

		//Recognize the sucess of the request
		response.send(`User ${request.body.username} successfully registered!`)
	} else{
		response.send('Please input BOTH username and password.');
	}

	// Mini-Activity, If the body.password and body.username is empty don't include in our mockdatabase thus if it's not empty add the username and password in out mock database

});

// This route expects to receive a PUT request at the URI "/change-password"
app.put("/change-password", (req, res) =>{

	let message;

	console.log(users.length);

	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){

		// If the username provided in th eclient/Postman and the username of the current object in th eloop is same, run the following code
		if(req.body.username == users[i].username){

			// Changes the users[i].password value to the req.body.password 
			users[i].password = req.body.password;

			//Changes the message to be sent back by the response
			message = `User ${req.body.username}'s password has been updated.`;
			console.log("Newly updated mock database:");
			console.log(users);

			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;

		// If no user was found
		} else {

			// Changes the message to be sent back by the response
			message = "User does not exist."
		}

	}


	//Send a response back to the client/Postman once the password has been updated or if a user is not found
	res.send(message);
})

app.listen(port, () => console.log( `Server is currently running at port ${port}`));