console.log("Hello World?");

// [ SECTION ] While Loop

	// A while loop takes in an expression/ condition
	// If condition evaluates to true, the statements in the code block will run.

	/*
	Syntax:
		while(expression/condition){
			statement;
		};
	*/

	function whileloop(){

		let count = 5;

		//While the value of count is not equal to 0
		while(count !== 0){

			// The current value of count is printed out.
			console.log("While: "+count);

			//Infinite loop, our program being written without the end. 
			// Make sure that expression/ conditions in loops have their corresponding increment/ decrement operators to stop the loop.

			count--;

			//Note:
			// Make sure that the corresponding increment/decrement is relevant to the condition/expression.
		};
	}


	// Another Example
	function gradeLevel(){
		let grade = 1;

		while(grade <= 5){
			console.log("I am a grade " + grade + "student!");

			grade++;
		}
	}

// [ SECTION ] Do While Loop

	// A do-while loop works alot like a while loop, but unlike while loops, do-while loops guarantee that the code will execute at least once.

	/*
	Syntax:
		do{
			statement
		} while(expression/condition)
	*/
	function doWhile(){
		let count = 20;

		do{
			console.log("Whatever happens, I will be there here");
			count--
		}while(count>0);
	};

	let number = Number(prompt("Give me a number"));

	function doWhile1(){
		do{

			// The current value of number is printed.
			console.log('Do While: '+number);

			//Increases the value of the number by 1 after every iteration to stop loop when it reaches the 10 or greater
			number +=2;
		} while(number<10);
	}

// [ SECTION ] For Loop

	// A for loop is more flexible than while and do-while loop
	// For Loop is consist of three parts:
		// 1. The "initialization" value that will track the progression of the loop
		// 2. The "expression/condition" that will be evaluated which will determine whether the loop will run one more time or not.
		// 3. The "finalExpression" indicates how to advances the loop/how the variable will behave.

	/*
	Syntax
		for(initialisation; expression/condition; finalExpression){
			statement
		} 
	*/

	function forLoop(){
		for (let count = 10; count <= 20; count++){
			console.log("You are currently: "+ count);
		};
	}

	// [SUB-SECTION] Loops for letters

	let myString = "alex";
	console.log(myString);
	console.log(myString.length);

		console.log(myString[0]);
		console.log(myString[1]);
		console.log(myString[2]);
		console.log(myString[3]);
		console.log(myString[4]);

		let x=0;

		for(x ; x < myString.length; x++){

			//The current variable of myString is printed out using its index value.
			console.log(myString[x])
		};

	//Mini-Activity
		// Create variable that contains your name
		// For every vowel in your name it should be replace with 0

		// Stretch Goal: Use prompt on collecting your name

	let name = prompt("Write your name: ");

	let i=0;
	for(i ; i < name.length; i++){

			// i = a value of number
			if (
				name[i].toLowerCase() === 'a' || 
				name[i].toLowerCase() === 'e' || 
				name[i].toLowerCase() === 'i' || 
				name[i].toLowerCase() === 'o' || 
				name[i].toLowerCase() === 'u')
			{
				console.log(0);
			} else{
				console.log(name[i]);
			};
		};


// [ SECTION ] Continue and 

	// The "continue" statement allows the code to go the next iteration of the loop without finishing the execution of all statements in a code block
	// The "break" statement is used to terminate the current loop once a match has been found

		for(let count = 0; count <= 20; count++){

			if (count % 2 ===0){
				//Tells the code to continue to the next iteration of the loop;
				// This ignores all statements located after the continue statement;
				continue;
			};
		};

		// The current value of number is printed out if the remainder is not equal to 0;
		console.log('Continue and Break: '+);




	// ANother example

		let name = 'alexandro';

		for (let i = 0; i <name.length; i++){
			console.log(name[i]);
			// If the vowel is equal to a, continue to the next iteration of the loop
			if(name[i].toLowerCase() === 'a'){
				console.log('Continue to the next iteration')
			}
		}
