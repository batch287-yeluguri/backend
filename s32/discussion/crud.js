let http = require('http');

//Mock Databse
let directory = [
		{
			"name": "Brandon",
			"email": "brandon@mail.com"
		},
		{
			"name": "Jobert",
			"email": "jobert@mail.com"
		}
	]
http.createServer(function (request, response){

	if(request.url == '/users' && request.method == 'GET'){

		response.writeHead(200, {'Content-Type': 'application/json'});
		// Sets the response output to JSON data type
		// We will convert the users array into JSON since the server return response in JSON format as well.
		//console.log(directory);
		response.write(JSON.stringify(directory));
		//console.log(JSON.stringify(directory));
		response.end();
	}

	if(request.url == '/users' && request.method == 'POST'){

		// 1. Initiate requestBody variable which will later contain the data/body from the Postman
		let requestBody = ' ';

		//2. Upon receiving the data, re-assign the value of requestBody to the contents od the body from Postman
		// Data is received from the client and is processed in the "data" stream 

		request.on('data', function(data){


			requestBody += data;
			console.log(requestBody);
		});


		//3. Before the request ends, convert the requestBody variable from string into JS object in order ro be able to access it properties and assign them into newUser variable
		request.on('end', function(){
			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			console.log(requestBody)

			// Create a new object representing the new mock database
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			// After setting the values from the requestBody to the newUser variable, push the newUser variable into the mock database 
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {"Content-Type": 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})
	}	


}).listen(4002);
console.log('Server is running at localhost: 4002');