let number = 2;
const getcube = number ** 3;

const anotherMessage = `The cube of ${number} is ${getcube}.`

console.log(anotherMessage);

let address =[ '258 Washington Ave NW', 'California', '90011'];
const [ address1, address2, address3 ] = address;

console.log(`I live at ${address1}, ${address2}  ${address3}`);

const animal = {
		name: 'Lolong',
		type: 'saltwater crocodile',
		weight: '1075 kgs',
		length: '20 ft 3 in',
	};
const { name, type, weight, length } = animal;
console.log(`${name} was a ${type}. He weighted at ${weight} with a measurement of ${length}.`);

let numbers = [1, 2, 3, 4, 5];



let numberForEach = numbers.forEach((number) => {
		console.log(number);
	});


let reduceNumber = numbers.reduce((x,y) => {
		return x + y;
	});

console.log(reduceNumber);

class Dog{
	properties(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
	let mydog = new Dog();

		mydog.name = "Frankie";
		mydog.age = 5;
		mydog.breed ="Miniature Dachshund";
console.log(mydog);